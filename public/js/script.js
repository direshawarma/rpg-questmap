$(document).ready(function () {
  const body = document.body;
  const map = document.getElementById("map");
  const mapImage = document.getElementById("mapImage");
  mapImage.src = "img/Barovia_Map.jpg";
  map.appendChild(mapImage);
  let q = $.getJSON('etc/quests.json', function (json) {
    return json
  })
  //console.log(q);
  eval(fetch("etc/quests.json")
    .then(response => response.json())
    .then(questlog => {
      for (var quest in questlog) {
        questdiv = document.createElement("div");
        text = document.createElement("span");
        icon = document.createElement("i");
        icon.className = questlog[quest]["icon"];
        text.innerText = quest;
        questdiv.className = "quest";
        map.appendChild(questdiv);
        questdiv.appendChild(icon);
        questdiv.appendChild(text)
        questlog[quest]["Events"].forEach(event => {
          eventdiv = document.createElement("div");
          eventdiv.className = "event";
          eventdiv.innerText = event["event"];
          eventdiv.style.left = event["xpos"];
          eventdiv.style.top = event["ypos"];
          questdiv.style.left = event["xpos"];
          questdiv.style.top = event["ypos"];
          questdiv.style.color = eventcolor(event["color"]);
          questdiv.appendChild(eventdiv);
        }); 

        

      }
    }));

});

function eventcolor(colorname) {
  switch (colorname ) {
    case "danger1": return "rgb(0,222,0)";
    case "danger2": return "rgb(222,222,0)";
    case "danger3": return "rgb(255,222,0)";
    case "danger4": return "rgb(255,128,64)";
    case "danger5": return "rgb(255,0,0)";
    case "turn-in": return "rgb(0,222,222)";
    case "fetch": return "rgb(0,128,255)";
    case "rumor": return "rgb(196,0,196)";
    default: return colorname;
  }
}